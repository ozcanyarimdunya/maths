import unittest

from maths import Math


class TestMath(unittest.TestCase):
    """
    Test class for Math
    """

    def test_add(self):
        """test for addition"""
        print("/n/n---- TEST ON LOCAL ----/n/n")
        self.assertEqual(Math.add(0, 0), 0)
        self.assertEqual(Math.add(1, 2), 3)
        self.assertEqual(Math.add(3, 2), 5)
        self.assertEqual(Math.add(10, 10), 20)

    def test_sub(self):
        """test for subtracting"""
        self.assertEqual(Math.sub(0, 0), 0)
        self.assertEqual(Math.sub(1, 2), -1)
        self.assertEqual(Math.sub(3, 2), 1)
        self.assertEqual(Math.sub(10, 10), 0)

    def test_mul(self):
        """test for multiplying"""
        self.assertEqual(Math.mul(0, 0), 0)
        self.assertEqual(Math.mul(1, 2), 2)
        self.assertEqual(Math.mul(3, 2), 6)
        self.assertEqual(Math.mul(10, 10), 100)

    def test_div(self):
        """test for division"""
        with self.assertRaises(ZeroDivisionError):
            self.assertEqual(Math.div(0, 0), 0)
        self.assertEqual(Math.div(1, 2), .5)
        self.assertEqual(Math.div(3, 2), 1.5)
        self.assertEqual(Math.div(10, 10), 1)


if __name__ == '__main__':
    unittest.main()
