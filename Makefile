.PHONY: help

help:
	@echo ""
	@echo " Math"
	@echo " ~~~~"
	@echo ""
	@echo " Please use one below options"
	@echo ""
	@echo " coverage      : Make code coverage and get the coverage report"
	@echo " coverage-html : Make code coverage report as html"
	@echo " install       : Install python dependencies"
	@echo " test          : Run unittests"
	@echo ""

coverage:
	@echo "Running coverage"
	@coverage run -m unittest discover
	@coverage report -m

coverage-html:
	@echo "Running coverage html report"
	coverage html

install:
	@echo "Installing python dependencies"
	@pip install -r requirements.txt

test:
	@echo "Running unittests"
	@python -m unittest discover