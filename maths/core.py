class Math:
    """
    Math
    ~~~~
    A basic maths operations class

    Usage:
    >>> Math.add(1, 2)
    3
    >>> Math.sub(1, 2)
    -1
    >>> Math.mul(1, 2)
    2
    >>> Math.div(1, 2)
    0.5
    """

    @staticmethod
    def add(x: [int, float], y: [int, float]) -> [int, float]:
        """
        Add given 2 numbers
        :param x:
        :param y:
        :return: x + y
        """
        return x + y

    @staticmethod
    def sub(x: [int, float], y: [int, float]) -> [int, float]:
        """
        Subtract given 2 numbers
        :param x:
        :param y:
        :return: x - y
        """
        return x - y

    @staticmethod
    def mul(x: [int, float], y: [int, float]) -> [int, float]:
        """
        Multiply given 2 numbers
        :param x:
        :param y:
        :return: x * y
        """
        return x * y

    @staticmethod
    def div(x: [int, float], y: [int, float]) -> [int, float]:
        """
        Divide given 2 numbers
        :param x:
        :param y:
        :return: x / y
        """
        return x / y
