from .core import Math

__all__ = (
    'Math',
)

__author__ = '@ozcanyarimdunya'
__version__ = '1.0.0'
