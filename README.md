# Maths

A basic math operations

## Installation

```shell script
make install
```

Make sure it is installed correctly, you should see current version **1.0.0**

```shell script
python -c "import maths; print(maths.__version__)"
```

## Testing

Unittest

```shell script
make test
```

Coverage

```shell script
make coverage
```

## Usage

Import the `Math` class from `maths` module, and start using its methods in your file

**main.py**
```python
from maths import Math

Math.add(1, 2)  # 1 + 2 = 3
Math.sub(1, 2)  # 1 - 2 = -1
Math.mul(1, 2)  # 1 * 2 = 2
Math.div(1, 2)  # 1 / 2 = 0.5
```
